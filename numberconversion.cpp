#include "numberconversion.h"
#include <iostream>

int romantoint(std::string roman_numeral)
{
	int values[] = { 1, 5, 10, 100, 500, 1000 };
	char letters[] =  { 'I', 'V', 'X', 'C', 'D', 'M'  };
	const int SIZE = roman_numeral.length();
	int input_values[SIZE];
	//the value of each roman numeral is stored in an array:
	for (int i = 0; i < SIZE; i++) {
		for (int j = 0; j < sizeof(letters); j++) {
			if (roman_numeral[i] == letters[j]) {
				input_values[i] = values[j];
			}
		}	
	}

	int result = input_values[SIZE - 1];
	for (int i = SIZE - 2; i >= 0; i--) {
		if (input_values[i] < input_values[i + 1]) {
			result = result - input_values[i];
		} else {
			result = result + input_values[i];
		}
	}

  return result;
}

std::string inttoroman(int number)
{
	int values[] = { 1, 5, 10, 100, 500, 1000 };
	std::string letters[] =  { "I", "V", "X", "C", "D", "M"  };
	std::string result = "";
	int sizeValues = sizeof(values)/sizeof(values[0]);

	for (int i = sizeValues - 1; i >= 0; i--) {
		while (values[i] <= number) {
			result = result + letters[i];
			number = number - values[i];
		}
	}

  return result;
}
