CXX = g++
CXXFLAGS = -g -Wall -Wextra -Wpedantic

converter : numberconversion.o romandigitconverter.cpp
	$(CXX) $(CXXFLAGS) -o $@ $^

numberconversion.o : numberconversion.cpp numberconversion.h
	$(CXX) $(CXXFLAGS) -c $<

.PHONY : clean

clean :
	$(RM) *.o
	$(RM) converter

